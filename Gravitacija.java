import java.util.*;
import java.lang.*;

public class Gravitacija{
	public static void main (String[] args){
		//System.out.println(OIS je zakon!);
		
		Scanner sc = new Scanner(System.in);
		
		double C = 6.674*(Math.pow(10, -11)); // gravitacijska konstanta
		double M = 5.972*(Math.pow(10, 24)); // masa Zemlje
		double R = 6.371*(Math.pow(10, 6)); //polmer zemle
		
		int v = sc.nextInt(); //nadmorska visina
		
		double a = (C*M)/(Math.pow(R+v, 2));
		
		//System.out.println(a);
		izpis(a, v);
	}
	
	public static void izpis(double pospesek, double visina){
		System.out.println("Podana nadmorska visina: " + visina + " Gravitacijski pospesek: " + pospesek);
	}
}
